//
//  ViewController.swift
//  iBeacon-AquaInfo
//
//  Created by Nabhat Yuktadatta on BE2559/06/24.
//  Copyright © 2559年 Nabhat Inc. All rights reserved.
//

import UIKit
import CoreLocation //To interact with iBeacon

class ViewController: UIViewController, CLLocationManagerDelegate, UIWebViewDelegate {
    
   // @IBOutlet weak var uuidLabel: UILabel!
   // @IBOutlet weak var majorlabel: UILabel!
   // @IBOutlet weak var minorLabel: UILabel!
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var webActivityIndicator: UIActivityIndicatorView!
    
    // To store aquarium infomation from seleted cell
    var aquarium: AquaBeacon?
    
    
    // MARK: Action
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabBarController?.tabBar.userInteractionEnabled = false
        
        webView.delegate = self
        
        
        if let aquarium = aquarium {
            //Set label to be same as selected aquarium
            print("Show information of Minor = \(aquarium.minor)")
           // uuidLabel.text = "Welcome to \(aquarium.UUID) Aquarium"
           // majorlabel.text = "\(aquarium.major)"
           // minorLabel.text = "\(aquarium.minor)"
        }
        
        let url = NSURL (string: "http://192.168.68.32:8080");
        //let url = NSURL (string: "http://10.10.1.17:8080");
        let requestObj = NSURLRequest(URL: url!);
        
        print("did load: \(requestObj)")
        
        webView.loadRequest(requestObj);
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    func webViewDidStartLoad(webView: UIWebView) {
        webActivityIndicator.startAnimating()
    }
    
    func webViewDidFinishLoad(webView: UIWebView) {
        webActivityIndicator.stopAnimating()
        
        //let script = "textChange( \(aquarium!.minor) )"
        //let script = "testing()"
        let script = "getAquariumData( \(aquarium!.minor) )"
        
        //if let returnedString = webView.stringByEvaluatingJavaScriptFromString("alert('Hello')") {
        if let returnedString = webView.stringByEvaluatingJavaScriptFromString( script ) {
            print("inject script  -> \(script)")
            //print("The result is \(returnedString)")
        }

    }
    
    
    override func shouldAutorotate() -> Bool {
        
        
        return false
    }
    
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        return [UIInterfaceOrientationMask.Portrait ]
    }

    


}

