//
//  AquaBeaconTableViewController.swift
//  iBeacon-AquaInfo
//
//  Created by Nabhat Yuktadatta on BE2559/06/24.
//  Copyright © 2559年 Nabhat Inc. All rights reserved.
//

import UIKit
import CoreLocation //To interact with iBeacon

class AquaBeaconTableViewController: UITableViewController, CLLocationManagerDelegate {
    
    // MARK: Beacon related Properties
    let locationManager = CLLocationManager()
    
    // Aqua-tan region
    let region = CLBeaconRegion(proximityUUID: NSUUID(UUIDString: "b76540e0-ef3c-11e4-95c9-0002a5d5c51b")!, identifier: "Aquatan") //set our region to search for only iBeacon with specify UUID
    
    let colors = [ 0: UIColor(red: 168/255, green: 252/255, blue: 0/255, alpha: 1.0) ]
    let darkCyan = UIColor(red: 18/255, green: 169/255, blue: 171/255, alpha: 1.0)
    let desaturatedBlue = UIColor(red: 57/255, green: 66/255, blue: 99/255, alpha: 1.0)
    let darkDesaturatedBlue = UIColor(red: 32/255, green: 36/255, blue: 63/255, alpha: 1.0)
    
    
    // MARK: Properties
    var aquaBeacons = [AquaBeacon]()
    var uuid = "HACKED by MOMO"
    var major = "HACKED by MOMO"
    var minor = "HACKED by MOMO"
    
    var aquariumShown = [Bool]()
    
    var timer       = NSTimer()
    
    
    // MARK: Initializer

    override func viewDidLoad() {
        super.viewDidLoad()
        //self.tableView.backgroundColor = UIColor.lightGrayColor()
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.None
        
        navigationController!.navigationBar.barTintColor = darkCyan
        navigationController!.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
        tabBarController!.tabBar.barTintColor = darkCyan
        tabBarController!.tabBar.tintColor = UIColor.whiteColor()
        
        
        
        
        
        
        
        // Do any additional setup after loading the view, typically from a nib.
        locationManager.delegate = self
        if(CLLocationManager.authorizationStatus() !=  CLAuthorizationStatus.AuthorizedWhenInUse) {
            locationManager.requestWhenInUseAuthorization()
        }
        locationManager.startRangingBeaconsInRegion(region)
        
        
        // Load the sample data.
        //loadSampleMeals()
    }
    
    func loadSampleMeals() {
        
        // This is mock up beacon
        let icon1 = UIImage(named: "Aqua01")!
        let aqua1 = AquaBeacon(UUID: "<UUID>", major: 99, minor: 99, icon: icon1)
        
        aquaBeacons += [aqua1]
        aquariumShown.append(false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: Beacon Detector
    //this function will recieve every iBeacon found as an array(beacons)
    func locationManager(manager: CLLocationManager, didRangeBeacons beacons: [CLBeacon], inRegion region: CLBeaconRegion) {
        
        //print("========================================")
        //print("LIST OF FOUND BEACON: ")
        
        
        for foundBeacon in beacons {
          //   print("minor = \(foundBeacon.minor.integerValue), prox = \(foundBeacon.proximity.rawValue)")
        }

        let knownBeacons =  beacons.filter{ $0.proximity != CLProximity.Unknown && $0.proximity == CLProximity.Near || $0.proximity == CLProximity.Immediate } // Filter out the unknow iBeacon (prox = 0)
        
        
       // print("\nLIST OF FILTER BEACON: ")
        
        for known in knownBeacons {
        //    print("minor = \(known.minor.integerValue), prox = \(known.proximity.rawValue)")
        }
        
        
        if(knownBeacons.count > 0) { //Found at least 1 beacon.
            
            // Add in table
            for eachBeacon in knownBeacons {
                //print("in this if")
                
                if notInFoundArray( eachBeacon ) { // If no duplicate found, add that beacon to 'aqua array'
                    //print("- ADD TO ARRAY ( \(eachBeacon.minor.integerValue) )")
                    let inRangeAB = AquaBeacon(UUID: eachBeacon.proximityUUID.UUIDString , major: eachBeacon.major.integerValue, minor: eachBeacon.minor.integerValue, icon: UIImage(named: "Aqua01")!)
                    
                    let newIndexPath = NSIndexPath(forRow: aquaBeacons.count, inSection: 0)
                    aquaBeacons.append(inRangeAB) //Add new aquarium to array
                    aquariumShown.append(false) // Set shown to false (not shown yet)
                    tableView.insertRowsAtIndexPaths([newIndexPath], withRowAnimation: .Bottom)
                }
                
            }
        
        } else {
            //print("No beacon found!")
        }
        
        
        //print("\nLIST OF AQUA ARRAY \nAquaArray(dont count mockup) = \(aquaBeacons.count-1)")
        
        for aqua in aquaBeacons {
            //print("# \(aqua.minor)")
        }
        //print("======================================== \n\n\n\n\n")
        
        
        
        
       // Delete from table
        // the beacons array will delete beacon which is not found(prox = 0) for 10 seconds
        for existBeacon in aquaBeacons {
            if !foundAquaBeaconWithArray(existBeacon, arrayBeacon: beacons) {
                        
                let indexed = aquaBeacons.indexOf{
                    $0.minor == existBeacon.minor
                }
                        
                //print("delete index: \(indexed!) = #\(aquaBeacons[indexed!].minor)")
                        
                aquaBeacons.removeAtIndex( indexed! )
                aquariumShown.removeAtIndex( indexed! )
                        
                let indexPath = NSIndexPath(forItem: indexed!, inSection: 0)
                tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
  
            }
        
        }
        
    }
    
    
    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aquaBeacons.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cellIdentifier = "AquaBeaconTableViewCell"
        let cell = tableView.dequeueReusableCellWithIdentifier( cellIdentifier, forIndexPath: indexPath) as! AquaBeaconTableViewCell
        
        cell.backgroundColor = UIColor.clearColor()

        let whiteFrameWidth =  self.view.frame.size.width - 20
        let whiteFrameHeight = self.view.frame.size.height/5 - 20
        
        let whiteRoundedView : UIView = UIView(frame: CGRectMake(10, 8, whiteFrameWidth, whiteFrameHeight))
        
        //whiteRoundedView.layer.backgroundColor = CGColorCreate(CGColorSpaceCreateDeviceRGB(), [1.0, 1.0, 1.0, 0.8])
        whiteRoundedView.layer.backgroundColor = desaturatedBlue.CGColor
        whiteRoundedView.layer.masksToBounds = false
        whiteRoundedView.layer.cornerRadius = 10.0
        whiteRoundedView.layer.shadowOffset = CGSizeMake(-1, 1)
        whiteRoundedView.layer.shadowOpacity = 0.2
        
        cell.contentView.addSubview(whiteRoundedView)
        cell.contentView.sendSubviewToBack(whiteRoundedView)
        
 
        // Configure the cell...
        let aquaBeacon = aquaBeacons[indexPath.row]
        
        let aquariumName = iBeaconIdentifier(aquaBeacon.minor)
        
        //cell.UUIDLabel.text = aquaBeacon.UUID
        aquaBeacon.UUID = aquariumName
        cell.UUIDLabel.text = aquariumName
        cell.majorLabel.text = "Major ID: \(aquaBeacon.major)"
        cell.minorLabel.text = "Minor ID: \(aquaBeacon.minor)"
        

        return cell
    }
    
    
    
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }


    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            aquaBeacons.removeAtIndex(indexPath.row) //delete from source data
            aquariumShown.removeAtIndex(indexPath.row)
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade) //delete from table
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    // MARK: - Animate The Table View Cell
    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        
        //cell.backgroundColor = desaturatedBlue
        
        if aquariumShown[indexPath.row] == false { // if aquarium haven't shown yet
            // First (set up before animate)
            cell.alpha = 0
            let rotationTransform = CATransform3DTranslate(CATransform3DIdentity /*Current position*/ , -800 , 0, 0) // swift left 500 point
            cell.layer.transform = rotationTransform //transform cell position
        
            // Do animation
            UIView.animateWithDuration(0.5) { // within 1 second
                cell.alpha = 1
                cell.layer.transform = CATransform3DIdentity // Cell move to the same position
            }
            
            aquariumShown[indexPath.row] = true
        }
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    // This method likely to pass data from table view (cell) to next page 
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        print("Trigger segue = \(segue.identifier)" )
        if segue.identifier == "ShowDetail" {
            let aquaInfoViewController = segue.destinationViewController as! ViewController
            
            // Get the cell that generated this segue.
            if let selectedCell = sender as? AquaBeaconTableViewCell {
                let indexPath = tableView.indexPathForCell(selectedCell)!
                let selectedAqua = aquaBeacons[indexPath.row]
                aquaInfoViewController.aquarium = selectedAqua
            }
        }
        else {
            print("Impossible")
        }
    }
    
    
    // MARK: Function
    func notInFoundArray( thatBeacon: CLBeacon ) -> Bool{
        for aqua in aquaBeacons {
            if thatBeacon.major.integerValue == aqua.major && thatBeacon.minor.integerValue == aqua.minor {
                    return false // Duplicate found (already add that beacon in 'aquaBeacons'
                
            }
        }
        
        return true // No duplicate found
    
    }
    
    func foundAquaBeaconWithArray ( aquaBeacon: AquaBeacon, arrayBeacon: [CLBeacon] ) -> Bool {
        
        for eachBeaconInArray in arrayBeacon {
            if aquaBeacon.minor == eachBeaconInArray.minor.integerValue {
                return true
            }
        }
        
        return false
    }
    

    func iBeaconIdentifier( beaconMinor: Int) -> String {
        switch beaconMinor {
            case 15001:
                return "AQUARIUM"
            case 15110:
                return "Mo"
            case 15109:
                return "June"
            case 15104:
                return "Masa-san"
            case 15090:
                return "Mori-san"
            case 15070:
                return "Sensei"
            case 15086:
                return "Matsui-san"
            case 303:
                return "Mo's iPhone"
            default:
                return "Unknown beacon"
        }
        
    }
    
    
    override func shouldAutorotate() -> Bool {
        
        
        return false
    }
    
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        return [UIInterfaceOrientationMask.Portrait ]
    }

    
    
    
}
