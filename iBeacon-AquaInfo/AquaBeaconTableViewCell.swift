//
//  AquaBeaconTableViewCell.swift
//  iBeacon-AquaInfo
//
//  Created by Nabhat Yuktadatta on BE2559/06/24.
//  Copyright © 2559年 Nabhat Inc. All rights reserved.
//

import UIKit

class AquaBeaconTableViewCell: UITableViewCell {
    
    // MARK: Properties
    @IBOutlet weak var UUIDLabel: UILabel!
    @IBOutlet weak var majorLabel: UILabel!
    @IBOutlet weak var minorLabel: UILabel!
    @IBOutlet weak var aquaImageView: UIImageView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
