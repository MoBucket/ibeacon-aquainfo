//
//  AquaBeacon.swift
//  iBeacon-AquaInfo
//
//  Created by Nabhat Yuktadatta on BE2559/06/24.
//  Copyright © 2559年 Nabhat Inc. All rights reserved.
//

import UIKit

class AquaBeacon {
    
    // MARK: Properties
    
    var UUID: String
    var major: Int
    var minor: Int
    
    var icon: UIImage?
    //var name: String
    //var info: String
    
    
    // MARK: Initialization
    
    init(UUID: String, major: Int, minor: Int, icon: UIImage) {
        // Initialize stored properties.
        self.UUID = UUID
        self.major = major
        self.minor = minor
        self.icon = icon
        
    }
}
