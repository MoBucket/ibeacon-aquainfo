//
//  RadarViewController.swift
//  iBeacon-AquaInfo
//
//  Created by Nabhat Yuktadatta on BE2559/08/09.
//  Copyright © 2559年 Nabhat Inc. All rights reserved.
//

import UIKit
import Darwin
import CoreLocation //To interact with iBeacon

class RadarViewController: UIViewController, CLLocationManagerDelegate  {
    
    // MARK: Beacon related Properties
    let locationManager = CLLocationManager()
    let region = CLBeaconRegion(proximityUUID: NSUUID(UUIDString: "b76540e0-ef3c-11e4-95c9-0002a5d5c51b")!, identifier: "Aquatan") //set our region to search for only iBeacon with specify UUID
    
    // MARK: Attributes
    var beaconIcon: UIView?
    var player: UIView?
    var aquaBeacons = [AquaBeacon]()
    var aquariumShown = [Bool]()
    var beaconIcons = [UIView]()
    var gridArr = [[Int]]()
    var beaconWithItsRSSI = [Int: Int]()
    let screenWidth = min(Int( UIScreen.mainScreen().bounds.size.width ), Int( UIScreen.mainScreen().bounds.size.height ))
    let screenHeight = max(Int( UIScreen.mainScreen().bounds.size.width ), Int( UIScreen.mainScreen().bounds.size.height ))
    
    //var initialPos = 50
    let iconSize = 40
    let pi = M_PI
    
    let aquatanImage = UIImage(named: "Aqua_pink")
    let darkCyan = UIColor(red: 18/255, green: 169/255, blue: 171/255, alpha: 1.0)
    
    // MARK: Action
    override func viewDidLoad() {
        super.viewDidLoad()
        print("Radar view did load")

        
        navigationController!.navigationBar.barTintColor = darkCyan
        navigationController!.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
        
        gridArr = Array(count: screenWidth, repeatedValue: Array(count: screenHeight, repeatedValue: 1))

        
        print("center X = \(self.view.center.x)")
        print("center Y = \(self.view.center.y)")
        
        let playerPosition = self.view.center
        let playerRadius = 10
        
        let circlePath = UIBezierPath(arcCenter: playerPosition, radius: CGFloat(playerRadius), startAngle: CGFloat(0), endAngle:CGFloat(M_PI * 2), clockwise: true)

        
        for x in (Int(playerPosition.x)-playerRadius)...(Int(playerPosition.x)+playerRadius) {
            for y in (Int(playerPosition.y)-playerRadius)...(Int(playerPosition.y)+playerRadius) {
                gridArr[x][y] = 0
            }
        }

        
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = circlePath.CGPath
        
        //change the fill color
        shapeLayer.fillColor = UIColor.redColor().CGColor
        //you can change the stroke color
        shapeLayer.strokeColor = UIColor.redColor().CGColor
        //you can change the line width
        shapeLayer.lineWidth = 3.0
        
        //self.view.addSubview(beaconIcon!)
        view.layer.addSublayer(shapeLayer)
        
        
        drawRadar( playerPosition )
        
        
        // Beacon ranging set up
        locationManager.delegate = self
        if(CLLocationManager.authorizationStatus() !=  CLAuthorizationStatus.AuthorizedWhenInUse) {
            locationManager.requestWhenInUseAuthorization()
        }
        locationManager.startRangingBeaconsInRegion(region)
    }
    
    // MARK: Beacon Detector
    //this function will recieve every iBeacon found as an array(beacons)
    func locationManager(manager: CLLocationManager, didRangeBeacons beacons: [CLBeacon], inRegion region: CLBeaconRegion) {
        print("RADAR===================================")


        print("LIST OF FOUND BEACON: ")
        
        
        for foundBeacon in beacons {
            print("minor = \(foundBeacon.minor.integerValue), prox = \(foundBeacon.proximity.rawValue), rssi = \(foundBeacon.rssi)")
        }
        
        let knownBeacons =  beacons.filter{ $0.proximity != CLProximity.Unknown && $0.proximity == CLProximity.Near || $0.proximity == CLProximity.Immediate } // Filter out the unknow iBeacon (prox = 0)
        
        
        print("\nLIST OF FILTER BEACON: ")
        
        for known in knownBeacons {
            print("minor = \(known.minor.integerValue), prox = \(known.proximity.rawValue)")
        }
        
        
        if(knownBeacons.count > 0) { //Found at least 1 beacon.
            
            // Add in table
            for eachBeacon in knownBeacons {
               
                if notInFoundArray( eachBeacon ) { // If no duplicate found, add that beacon to 'aqua array'
                    print("- ADD TO ARRAY ( \(eachBeacon.minor.integerValue) )")
                    let inRangeAB = AquaBeacon(UUID: eachBeacon.proximityUUID.UUIDString , major: eachBeacon.major.integerValue, minor: eachBeacon.minor.integerValue, icon: UIImage(named: "Aqua01")!)
                    let distance = Double(eachBeacon.rssi*(-1)*3)
                    let prox = eachBeacon.proximity.rawValue
                    
                    aquaBeacons.append(inRangeAB) //Add new aquarium to array
                    aquariumShown.append(false) // Set shown to false (not shown yet)
                   // print("beacon --> \(eachBeacon.rssi)")
                    
                    print("draw beacon")
                    beaconIcons.append( drawBeacon( inRangeAB, distance: distance, prox: prox) )
                    beaconWithItsRSSI[eachBeacon.minor.integerValue] = eachBeacon.rssi

                }
                
            }
            
        } else {
            print("No beacon found!")
        }
        
        
        print("\nLIST OF AQUA ARRAY \nAquaArray(dont count mockup) = \(aquaBeacons.count-1)")
        
        for aqua in aquaBeacons {
            print("# \(aqua.minor)")
        }
        print("======================================== \n\n\n\n\n")
        
        
        
        
        // Delete from table
        // the beacons array will delete beacon which is not found(prox = 0) for 10 seconds
        for existBeacon in aquaBeacons {
            if !foundAquaBeaconWithArray(existBeacon, arrayBeacon: beacons) {
                
                let indexed = aquaBeacons.indexOf{
                    $0.minor == existBeacon.minor
                }
                
                print("Delete(from Radar) index: \(indexed!) = #\(aquaBeacons[indexed!].minor)")
                
                beaconWithItsRSSI[aquaBeacons[indexed!].minor] = nil
                aquaBeacons.removeAtIndex( indexed! )
                aquariumShown.removeAtIndex( indexed! )
                eraseBeacon( indexed! )
                
                
                //let indexPath = NSIndexPath(forItem: indexed!, inSection: 0)
                
            }
            
        }
        
        updateIconPosition( knownBeacons )

    }
    
    // MARK: Function
    func notInFoundArray( thatBeacon: CLBeacon ) -> Bool{
        
        for aqua in aquaBeacons {
            if thatBeacon.major.integerValue == aqua.major && thatBeacon.minor.integerValue == aqua.minor {
               
                return false // Duplicate found (already add that beacon in 'aquaBeacons'
                
            }
        }
        
        return true // No duplicate found
        
    }
    
    func foundAquaBeaconWithArray ( aquaBeacon: AquaBeacon, arrayBeacon: [CLBeacon] ) -> Bool {
        
        for eachBeaconInArray in arrayBeacon {
            if aquaBeacon.minor == eachBeaconInArray.minor.integerValue {
                return true
            }
        }
        
        return false
    }
    
    func drawBeacon( aquaBeacon: AquaBeacon, distance: Double, prox: Int) -> UIView{
        
        print("receive DISTANCE = \(distance), PROX = \(prox)")
        
        //screen size
        let bounds = UIScreen.mainScreen().bounds
        let width = bounds.size.width
        let height = bounds.size.height
        
        
        print("*************************** Screen width = \(width)") //iPad width = 1024.0
        print("*************************** Screen height = \(height)") //iPad height = 768.0
        print("Arr width = \(gridArr.count)")
        print("Arr height = \(gridArr[0].count)")
        //print("Dubug point = \(gridArr[356][768])")
        
        //let barHeight = 70
        //let padding = 80
        var posX = CGFloat(0)
        var posY = CGFloat(0)
        
        var angle = 0.0
    
        repeat {
   
        angle = drand48()*pi*2
        posX = CGFloat( Int( cos(angle)*distance )+Int(self.view.center.x) )
        posY = CGFloat( Int( sin(angle)*distance )+Int(self.view.center.y) )
            
        print("random point at ( \(posX), \(posY) )")
        //print("valid = \(isValidToDraw( Int(posX), startY: Int(posY), endX: Int(posX)+iconSize, endY: Int(posY)+iconSize))")
            
        } while !isValidToDraw( Int(posX), startY: Int(posY), endX: Int(posX)+iconSize, endY: Int(posY)+iconSize)
        
        
        
        
        //initialPos += 120
        let dimen = CGRectMake( posX , posY, CGFloat(iconSize), CGFloat(iconSize) ) //last two is width and length
        
        drawGrid( Int(posX), startY: Int(posY), endX: Int(posX)+iconSize, endY: Int(posY)+iconSize )
        print("draw icon at point ( \(posX), \(posY) )")
 
        let newBeaconIcon = UIButton(frame: dimen)
        newBeaconIcon.setImage(aquatanImage, forState: .Normal)
        newBeaconIcon.setTitle("\(iBeaconIdentifier(aquaBeacon.minor))", forState: .Normal)
        newBeaconIcon.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        newBeaconIcon.tag = aquaBeacon.minor
        newBeaconIcon.adjustsImageWhenHighlighted = false


        setupButton(newBeaconIcon)
        
        newBeaconIcon.addTarget(self, action: #selector(RadarViewController.beaconIconTapped(_:)), forControlEvents: .TouchDown)
        
        self.view.addSubview(newBeaconIcon)
        
        return newBeaconIcon
    }
    
    func eraseBeacon( indexed: Int ) {
        //initialPos -= 120
        print("Delete from position x = \(beaconIcons[indexed].frame.origin.x), and y = \(beaconIcons[indexed].frame.origin.y)")
        let originX = Int( beaconIcons[indexed].frame.origin.x )
        let originY = Int( beaconIcons[indexed].frame.origin.y )
        
        eraseGrid(originX, startY: originY, endX: originX+iconSize, endY: originY+iconSize)
        beaconIcons[indexed].removeFromSuperview()
        beaconIcons.removeAtIndex(indexed)
        
    }
    
    func beaconIconTapped(button: UIButton) {
        
        //bouncing animation
        UIView.animateWithDuration(0.1 ,
            animations: {
                button.transform = CGAffineTransformMakeScale(0.6, 0.6)
            },
            completion: { finish in
                UIView.animateWithDuration(0.1){
                button.transform = CGAffineTransformIdentity
            }
        })
        
        print("Button pressed 👍")
        self.performSegueWithIdentifier("ShowDetail", sender: button)
    }
    

    func isValidToDraw(startX: Int, startY: Int, endX: Int, endY: Int) -> Bool {
        //print("start at (\(startX), \(startY)), end at (\(endX), \(endY))")
        var result = true

        for x in startX...endX {
            for y in startY...endY {
                //print("(\(x),\(y)) = \(gridArr[x][y])+")
                if gridArr[x][y] == 0 {
                    result = false
                }
            }
        }
        return result
    }
    
    func drawGrid(startX: Int, startY: Int, endX: Int, endY: Int) {
        print("Draw grid at (\(startX), \(startY)), end at (\(endX), \(endY))")
        
        for x in startX...endX {
            for y in startY...endY {
                gridArr[x][y] = 0
            }
        }
    }
    
    func eraseGrid(startX: Int, startY: Int, endX: Int, endY: Int) {
        print("Erase grid at (\(startX), \(startY)), end at (\(endX), \(endY))")
        
        for x in startX...endX {
            for y in startY...endY {
                gridArr[x][y] = 1
            }
        }
    }
    
    func updateIconPosition( knownBeacon: [CLBeacon] ) {
        for eachBeacon in knownBeacon {
            if let recoredRSSI = beaconWithItsRSSI[ eachBeacon.minor.integerValue ] {
                //print("recored = \(recoredRSSI)")
                if !(recoredRSSI-3 < eachBeacon.rssi && eachBeacon.rssi < recoredRSSI+3) {
                    
                    let indexed = beaconIcons.indexOf{
                        $0.tag == eachBeacon.minor.integerValue
                    }
                    
                    var posX = CGFloat(0)
                    var posY = CGFloat(0)
                    var angle = 0.0
                    var distance = Double(eachBeacon.rssi*(-1)*3)

                    repeat {
                        angle = drand48()*pi*2
                        posX = CGFloat( Int( cos(angle)*distance )+Int(self.view.center.x) )
                        posY = CGFloat( Int( sin(angle)*distance )+Int(self.view.center.y) )
                        
                        print("**change beacon \(eachBeacon.minor.integerValue) to point ( \(posX), \(posY) ) with rssi = \(eachBeacon.rssi)")
                        //print("valid = \(isValidToDraw( Int(posX), startY: Int(posY), endX: Int(posX)+iconSize, endY: Int(posY)+iconSize))")
                        
                    } while !isValidToDraw( Int(posX), startY: Int(posY), endX: Int(posX)+iconSize, endY: Int(posY)+iconSize)

                    
                    let originX = Int( beaconIcons[indexed!].frame.origin.x )
                    let originY = Int( beaconIcons[indexed!].frame.origin.y )
                    print("origin X,Y = (\(originX), \(originY))")
                    eraseGrid(originX, startY: originY, endX: originX+iconSize, endY: originY+iconSize)
                
                    drawGrid( Int(posX), startY: Int(posY), endX: Int(posX)+iconSize, endY: Int(posY)+iconSize )
                    
                    beaconIcons[indexed!].frame = CGRectMake( posX, posY, beaconIcons[indexed!].frame.size.width, beaconIcons[indexed!].frame.size.height );
                    // set new position exactly

                }
            }
        }
    }

    func setupButton(button: UIButton) {
        let spacing: CGFloat = 0.0
        let imageSize: CGSize = button.imageView!.image!.size
        button.titleEdgeInsets = UIEdgeInsetsMake(0.0, -imageSize.width, -(imageSize.height + spacing), 0.0)
        let labelString = NSString(string: button.titleLabel!.text!)
        let titleSize = labelString.sizeWithAttributes([NSFontAttributeName: button.titleLabel!.font])
        button.imageEdgeInsets = UIEdgeInsetsMake(-(titleSize.height + spacing), 0.0, 0.0, -titleSize.width)
        let edgeOffset = abs(titleSize.height - imageSize.height) / 2.0;
        button.contentEdgeInsets = UIEdgeInsetsMake(edgeOffset, 0.0, edgeOffset, 0.0)
        
        button.titleLabel!.numberOfLines = 1
        button.titleLabel!.adjustsFontSizeToFitWidth = true
        button.titleLabel!.lineBreakMode =  NSLineBreakMode.ByClipping
    }
    
    func drawRadar( center: CGPoint ) {
        let innerRadius = 40*2
        let mediumRadius = 60*3
        let farRadius = 80*4
        
        let InnerCirclePath = UIBezierPath(arcCenter: center, radius: CGFloat(innerRadius), startAngle: CGFloat(0), endAngle:CGFloat(M_PI * 2), clockwise: true)
        let InnerCircle = CAShapeLayer()
        InnerCircle.path = InnerCirclePath.CGPath
        
        //change the fill color
        InnerCircle.fillColor = UIColor.clearColor().CGColor
        //you can change the stroke color
        InnerCircle.strokeColor = UIColor.greenColor().CGColor
        //you can change the line width
        InnerCircle.lineWidth = 3.0
        
        
        let MediumCirclePath = UIBezierPath(arcCenter: center, radius: CGFloat(mediumRadius), startAngle: CGFloat(0), endAngle:CGFloat(M_PI * 2), clockwise: true)
        let MediumCircle = CAShapeLayer()
        MediumCircle.path = MediumCirclePath.CGPath
        
        //change the fill color
        MediumCircle.fillColor = UIColor.clearColor().CGColor
        //you can change the stroke color
        MediumCircle.strokeColor = UIColor.blueColor().CGColor
        //you can change the line width
        MediumCircle.lineWidth = 3.0
        
        
        let FarCirclePath = UIBezierPath(arcCenter: center, radius: CGFloat(farRadius), startAngle: CGFloat(0), endAngle:CGFloat(M_PI * 2), clockwise: true)
        let FarCircle = CAShapeLayer()
        FarCircle.path = FarCirclePath.CGPath
        
        //change the fill color
        FarCircle.fillColor = UIColor.clearColor().CGColor
        //you can change the stroke color
        FarCircle.strokeColor = UIColor.yellowColor().CGColor
        //you can change the line width
        FarCircle.lineWidth = 3.0
        
        
        
        view.layer.addSublayer(InnerCircle)
        view.layer.addSublayer(MediumCircle)
        view.layer.addSublayer(FarCircle)
    }
    
    func iBeaconIdentifier( beaconMinor: Int) -> String {
        switch beaconMinor {
        case 15001:
            return "AQUARIUM"
        case 15110:
            return "Mo"
        case 15109:
            return "June"
        case 15104:
            return "Masa-san"
        case 15090:
            return "Mori-san"
        case 15070:
            return "Sensei"
        case 15086:
            return "Matsui-san"
        case 303:
            return "Mo's iPhone"
        default:
            return "\(beaconMinor)"
        }
        
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    // This method likely to pass data from table view (cell) to next page
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        print("Hello Geoge! Trigger segue = \(segue.identifier)" )
        print("Sender =  \(sender)")
        if segue.identifier == "ShowDetail" {
            let aquaInfoViewController = segue.destinationViewController as! ViewController
            
            // Get the icon that generated this segue.
            if let selectedIcon = sender as? UIButton  {
                print("tag = \(selectedIcon.tag)")
                let indexed = aquaBeacons.indexOf{
                    $0.minor == selectedIcon.tag
                }

                let selectedAqua = aquaBeacons[indexed!]
                aquaInfoViewController.aquarium = selectedAqua
            }
        }
        else {
            print("Impossible")
        }
        
    }
    
    
    override func shouldAutorotate() -> Bool {
        return false
    }
    
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        return [UIInterfaceOrientationMask.Portrait ]
    }




}
